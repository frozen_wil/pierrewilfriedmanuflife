package com.toprank.manulife.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.toprank.manulife.model.Site;

@Repository
public interface SiteRepository extends JpaRepository<Site, Long> {

    Site findByName(String name);
    Site findById(Long id);

}
