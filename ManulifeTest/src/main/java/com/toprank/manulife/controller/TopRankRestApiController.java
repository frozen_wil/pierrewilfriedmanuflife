package com.toprank.manulife.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.toprank.manulife.model.Site;
import com.toprank.manulife.service.SiteService;
import com.toprank.manulife.util.SiteError;

@RestController
@RequestMapping("/api")
public class TopRankRestApiController {
	
	public static final Logger logger = LoggerFactory.getLogger(TopRankRestApiController.class);
	/**
	 * Allow to retrieve the service to work
	 */
	@Autowired
	SiteService siteService;

	/**
	 * Ge the list of all sites
	 * @return
	 */
	@RequestMapping(value = "/site/", method = RequestMethod.GET)
	public ResponseEntity<List<Site>> listAllSites() {
		List<Site> sites = siteService.findAllSites();
		if (sites.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			
		}
		return new ResponseEntity<List<Site>>(sites, HttpStatus.OK);
	}

	/**
	 * Get the site informations
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/site/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getSite(@PathVariable("id") long id) {
		logger.info("Fetching Site with id {}", id);
		Site site = siteService.findById(id);
		if (site == null) {
			logger.error("Site with id {} not found.", id);
			return new ResponseEntity(new SiteError("Site with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Site>(site, HttpStatus.OK);
	}

	/**
	 * Create the site with informations
	 * @param site
	 * @param ucBuilder
	 * @return
	 */

	@RequestMapping(value = "/site/", method = RequestMethod.POST)
	public ResponseEntity<?> createSite(@RequestBody Site site, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Site : {}", site);

		/*if (siteService.isSiteExist(site)) {
			logger.error("can not create. A Site with name {} already exist please insert another name", site.getName());
			return new ResponseEntity(new SiteError("can not  to create. A Site with name " + 
			site.getName() + " already exist."),HttpStatus.CONFLICT);
		}*/
		siteService.saveSite(site);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/site/{id}").buildAndExpand(site.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	/**
	 * Updated the site 
	 * @param id
	 * @param site
	 * @return
	 */
	@RequestMapping(value = "/site/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateSite(@PathVariable("id") long id, @RequestBody Site site) {
		logger.info("Updating Site with id {}", id);

		Site currentsite = siteService.findById(id);

		if (currentsite == null) {
			logger.error("can not  to update. Site with id {} not found.", id);
			return new ResponseEntity(new SiteError("can not  to upate. Site with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		currentsite.setName(site.getName());
		currentsite.setWeeklyDate(site.getWeeklyDate());
		currentsite.setTotalVisit(site.getTotalVisit());

		siteService.updateSite(currentsite);
		return new ResponseEntity<Site>(currentsite, HttpStatus.OK);
	}

/**
 * Remove the site from the list
 * @param id of the site to be deleted
 * @return
 */

	@RequestMapping(value = "/site/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteSite(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Site with id {}", id);

		Site site = siteService.findById(id);
		if (site == null) {
			logger.error("can not  delete. Site  id {} not found.", id);
			return new ResponseEntity(new SiteError("can not   delete. Site with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		siteService.deleteSiteById(id);
		return new ResponseEntity<Site>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all sites
	 * @return the site to be deleted
	 */
	@RequestMapping(value = "/site/", method = RequestMethod.DELETE)
	public ResponseEntity<Site> deleteAllSites() {
		logger.info("Deleting All Site");

		siteService.deleteAllSites();
		return new ResponseEntity<Site>(HttpStatus.NO_CONTENT);
	}

}