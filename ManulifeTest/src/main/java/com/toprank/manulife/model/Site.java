package com.toprank.manulife.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="Sites")
public class Site implements Serializable{

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	@Column(name="NAME", nullable=false)
	private String name;

	@Column(name="WEEKLYDATE", nullable=false)
	private Date weeklyDate;

	@Column(name="TOTALVISIT", nullable=false)
	private Long totalVisit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getWeeklyDate() {
		return weeklyDate;
	}

	public void setWeeklyDate(Date weeklyDate) {
		this.weeklyDate = weeklyDate;
	}

	public Long getTotalVisit() {
		return totalVisit;
	}

	public void setTotalVisit(Long totalVisit) {
		this.totalVisit = totalVisit;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Site site = (Site) o;

		if (Double.compare(site.totalVisit, totalVisit) != 0) return false;
		if (id != null ? !id.equals(site.id) : site.id != null) return false;
		if (name != null ? !name.equals(site.name) : site.name != null) return false;
		return weeklyDate != null ? weeklyDate.equals(site.weeklyDate) : site.weeklyDate == null;
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (weeklyDate != null ? weeklyDate.hashCode() : 0);
		temp = Double.doubleToLongBits(totalVisit);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "Site [id=" + id + ", name=" + name + ", weeklyDate=" + weeklyDate
				+ ", salary=" + totalVisit + "]";
	}


}
