package com.toprank.manulife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.toprank.manulife.configuration.JpaConfiguration;

/**
 * This class will load the configuration file, initialise the bean and launch the server
 *  
 * @author PIERRE
 *
 */
@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.toprank.manulife"})
public class TopRankSitesApp {

	public static void main(String[] args) {
		SpringApplication.run(TopRankSitesApp.class, args);
	}
}
