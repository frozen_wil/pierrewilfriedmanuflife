package com.toprank.manulife.util;


public class SiteError {

    private String messageError;

    public SiteError(String messageError){
        this.messageError = messageError;
    }

    public String getMessageError() {
        return messageError;
    }

}

