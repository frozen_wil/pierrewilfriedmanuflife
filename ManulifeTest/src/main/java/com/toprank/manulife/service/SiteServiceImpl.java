package com.toprank.manulife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.toprank.manulife.model.Site;
import com.toprank.manulife.repositories.SiteRepository;



@Service("SiteService")
@Transactional
public class SiteServiceImpl implements SiteService{

	@Autowired
	private SiteRepository siteRepository;

	public Site findById(Long id) {
		return siteRepository.findOne(id);
	}

	public Site findByName(String name) {
		return siteRepository.findByName(name);
	}

	public void saveSite(Site site) {
		siteRepository.save(site);
	}
	/**
	 * Update the site
	 */
	public void updateSite(Site site){
		saveSite(site);
	}
	/**
	 * delete the site by id
	 */
	public void deleteSiteById(Long id){
		siteRepository.delete(id);
	}
	/**
	 * Deleles all sites
	 */
	public void deleteAllSites(){
		siteRepository.deleteAll();
	}
	/**
	 * Find all sites
	 * @return
	 */
	public List<Site> findAllSites(){
		return siteRepository.findAll();
	}
	/**
	 * Check if the site exists
	 */
	public boolean isSiteExist(Site site) {
		return findById(site.getId()) != null;
	}

}
