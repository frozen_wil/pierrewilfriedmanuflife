package com.toprank.manulife.service;


import java.util.List;

import com.toprank.manulife.model.Site;

public interface SiteService {
	
	Site findById(Long id);

	Site findByName(String name);

	void saveSite(Site site);

	void updateSite(Site site);

	void deleteSiteById(Long id);

	void deleteAllSites();

	List<Site> findAllSites();

	boolean isSiteExist(Site site);
}