var app = angular.module('crudApp',['ui.router','ngStorage']);

app.constant('urls', {
    BASE: 'http://localhost:8080/TopRankSitesApp',
    SITE_SERVICE_API : 'http://localhost:8080/TopRankSitesApp/api/site/'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/list',
                controller:'SiteController',
                controllerAs:'ctrl',
                resolve: {
                    sites: function ($q, SiteService) {
                        console.log('Load all sites');
                        var deferred = $q.defer();
                        SiteService.loadAllSites().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    }]);
