'use strict';

angular.module('crudApp').factory('SiteService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
                loadAllSites: loadAllSites,
                getAllSites: getAllSites,
                getSite: getSite,
                createSite: createSite,
                updateSite: updateSite,
                removeSite: removeSite
            };

            return factory;

            function loadAllSites() {
                console.log('Fetching all sites');
                var deferred = $q.defer();
                $http.get(urls.SITE_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all sites');
                            $localStorage.sites = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading sites');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllSites(){
                return $localStorage.sites;
            }

            function getSite(id) {
                console.log('Fetching Site with id :'+id);
                var deferred = $q.defer();
                $http.get(urls.SITE_SERVICE_API + id)
                    .then(
                        function (response) {
                            console.log('Fetched successfully Site with id :'+id);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while loading site with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createSite(site) {
                console.log('Creating Site');
                var deferred = $q.defer();
                $http.post(urls.SITE_SERVICE_API, site)
                    .then(
                        function (response) {
                            loadAllSites();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                           console.error('Error while creating Site : '+errResponse.data.errorMessage);
                           deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function updateSite(site, id) {
                console.log('Updating Site with id '+id);
                var deferred = $q.defer();
                $http.put(urls.SITE_SERVICE_API + id, site)
                    .then(
                        function (response) {
                            loadAllSites();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while updating Site with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function removeSite(id) {
                console.log('Removing Site with id '+id);
                var deferred = $q.defer();
                $http.delete(urls.SITE_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllSites();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while removing Site with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

        }
    ]);

