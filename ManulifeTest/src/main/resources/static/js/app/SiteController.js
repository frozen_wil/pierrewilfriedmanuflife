'use strict';

angular.module('crudApp').controller('SiteController',
    ['SiteService', '$scope',  function( SiteService, $scope) {

        var self = this;
        self.site = {};
        self.sites=[];

        self.submit = submit;
        self.getAllSites = getAllSites;
        self.createSite = createSite;
        self.updateSite = updateSite;
        self.removeSite = removeSite;
        self.editSite = editSite;
        self.reset = reset;

        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;
        //For validation
        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;
        self.onlyDates = /^((\d{4})-(\d{2})-(\d{2})|(\d{2})\/(\d{2})\/(\d{4}))$/;
        
        function submit() {
            console.log('Submitting');
            if (self.site.id === undefined || self.site.id === null) {
                console.log('Saving New Site', self.site);
                createSite(self.site);
            } else {
                updateSite(self.site, self.site.id);
                console.log('Site updated with id ', self.site.id);
            }
        }

        function createSite(site) {
            console.log('About to create site');
            SiteService.createSite(site)
                .then(
                    function (response) {
                        console.log('Site created successfully');
                        self.successMessage = 'Site created successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.site={};
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Error while creating Site');
                        self.errorMessage = 'Error while creating Site: ' + errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }


        function updateSite(site, id){
            console.log('About to update site');
            SiteService.updateSite(site, id)
                .then(
                    function (response){
                        console.log('Site updated successfully');
                        self.successMessage='Site updated successfully';
                        self.errorMessage='';
                        self.done = true;
                        $scope.myForm.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while updating Site');
                        self.errorMessage='Error while updating Site '+errResponse.data;
                        self.successMessage='';
                    }
                );
        }


        function removeSite(id){
            console.log('About to remove Site with id '+id);
            SiteService.removeSite(id)
                .then(
                    function(){
                        console.log('Site '+id + ' removed successfully');
                    },
                    function(errResponse){
                        console.error('Error while removing site '+id +', Error :'+errResponse.data);
                    }
                );
        }


        function getAllSites(){
            return SiteService.getAllSites();
        }

        function editSite(id) {
            self.successMessage='';
            self.errorMessage='';
            SiteService.getSite(id).then(
                function (site) {
                    self.site = site;
                },
                function (errResponse) {
                    console.error('Error while removing site ' + id + ', Error :' + errResponse.data);
                }
            );
        }
        function reset(){
            self.successMessage='';
            self.errorMessage='';
            self.site={};
            $scope.myForm.$setPristine(); //reset Form
        }
        
        function DemoController($scope) {
            $scope.sites = SiteService.getAllSites();
            
            $scope.rangeSelectorOptions = {
                margin: {
                    top: 20
                },
                size: {
                    height: 40
                },
                dataSource: SiteService.getAllSites(),
                dataSourceField: "totalVisit",
                behavior: {
                    callValueChanged: "onMoving"
                },
                title: "Filter sites List by Total Visits",
                onValueChanged: function (e) {
                    var selectedSite = $.grep(SiteService.getAllSites(), function(oneSite) {
                        return oneSite.totalVisit >= e.value[0] && oneSite.totalVisit <= e.value[1];
                    });
                    $scope.oneSite = selectedSite;
                }
            };
        }
    }


    ]);