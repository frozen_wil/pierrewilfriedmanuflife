<!DOCTYPE html>

<html lang="en" ng-app="crudApp">
    <head>
        <title>${title}</title>
        <link href="css/bootstrap.css" rel="stylesheet"/>
        <link href="css/app.css" rel="stylesheet"/>
        <link href="css/datepicker.css" rel="stylesheet"/>
        <link href="css/inputdatapicker.css" rel="stylesheet"/>
        
    </head>
    <body>

        <div ui-view></div>
        <script src="js/lib/angular-locale_en-us.js"></script>
        <script src="js/lib/angular.min.js" ></script>
        <script src="js/lib/angular-ui-router.min.js" ></script>
        <script src="js/lib/localforage.min.js" ></script>
        <script src="js/lib/ngStorage.min.js"></script>
        <script src="js/app/app.js"></script>
        <script src="js/app/SiteService.js"></script>
        <script src="js/lib/datepicker.js"></script>
        <script src="js/app/SiteController.js"></script>
       
    </body>
</html>