<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead"> Add/Update WebSite </span></div>
		<div class="panel-body">
	        <div class="formcontainer">
	            <div class="alert alert-success" role="alert" ng-if="ctrl.successMessage">{{ctrl.successMessage}}</div>
	            <div class="alert alert-danger" role="alert" ng-if="ctrl.errorMessage">{{ctrl.errorMessage}}</div>
	            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
	                <input type="hidden" ng-model="ctrl.site.id" />
	                 <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="weeklyDate">Weekly date</label>
	                        <div class="col-md-7">
	                            <input id="weeklyDate" type="text" data-datepicker="{ theme: 'flat'}" ng-model="ctrl.site.weeklyDate" class="form-control input-sm" placeholder="Enter weekly Date." required ng-pattern="ctrl.onlyDates"/>
	                        </div>
	                    </div>
	                </div>
	
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="sitename">Web Site Name</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.site.name" id="sitename" class="sitename form-control input-sm" placeholder="Enter Website name" required ng-minlength="3"/>
	                        </div>
	                    </div>
	                </div>

	               
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="totalVisit">Total Visits</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.site.totalVisit" id="totalVisit" class="form-control input-sm" placeholder="Enter total Visits." required ng-pattern="ctrl.onlyNumbers"/>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-actions floatRight">
	                        <input type="submit"  value="{{!ctrl.site.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid || myForm.$pristine">
	                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
	                    </div>
	                </div>
	            </form>
    	    </div>
		</div>	
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label class="col-md-2 control-lable" for="searchBy">Search By :</label>
            
        </div>
    </div>
    
     <div class="row">
        <div class="form-group col-md-12">
            <label class="col-md-2 control-lable" for="weeklyDate">Select Weekly date</label>
            <div class="col-md-7">
                
                <input id="searchWeeklyDate" type="date" data-datepicker="{ theme: 'flat'}" ng-model="datePicker" class="form-control input-sm" placeholder="SearchEnter weekly Date."  ng-pattern="ctrl.onlyDates"/>
                Choosen date :{{datePicker| date: "yyyy-MM-dd"}}
             </div>
         </div>
      </div>

    
    <div class="row">
        <div class="form-group col-md-12">
            <label class="col-md-2 control-lable" for="searchByName">Select Web Site Name</label>
            <div class="col-md-7">
                <input type="text" ng-model="searchSiteName" id="searchByName" class="sitename form-control input-sm" placeholder="Website name" required ng-minlength="3"/>
            </div>
        </div>
    </div>
	<div range-slider="range-slider" 
         min="200" 
         max="300" 
         model-min="100" 
         model-max="400">
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead" align = "center" >The Top 5 Websites Ranking  </span></div>
		<div class="panel-body">
			<div class="table-responsive">
		        <table class="table table-hover">
				 <div class="demo-container" ng-app="crudApp" ng-controller="SiteController">
		        <div id="range-selector-demo">
		            <div dx-range-selector="rangeSelectorOptions"></div>
			            <thead>
		            <tr>
		                <th>ID</th>
		                <th>WEEKLY DATE</th>
		                <th>WEBSITE NAME</th>
		                <th>TOTAL VISITS</th>
		                <th width="100"></th>
		                <th width="100"></th>
		            </tr>
		            </thead>
		            <tbody>
		            <tr ng-repeat="oneSite in ctrl.getAllSites() | filter:{weeklyDate: (datePicker | date: 'yyyy-MM-dd')}| orderBy:'-totalVisit' | limitTo:5 | filter:{ name: searchSiteName }">
		                <td>{{oneSite.id}}</td>
		                <td>{{oneSite.weeklyDate}} </td>
		                <td>{{oneSite.name}}</td>
		                <td>{{oneSite.totalVisit}}</td>
		                <td><button type="button" ng-click="ctrl.editSite(oneSite.id)" class="btn btn-success custom-width">Edit</button></td>
		                <td><button type="button" ng-click="ctrl.removeSite(oneSite.id)" class="btn btn-danger custom-width">Remove</button></td>
		            </tr>
		            </tbody>
		             </div>
        
    </div>
		        </table>		
			</div>
		</div>
    </div>
</div>