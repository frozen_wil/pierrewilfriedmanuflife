/*
SQLyog Enterprise Trial - MySQL GUI v7.11 
MySQL - 5.7.17-log : Database - toprank
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`toprank` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `toprank`;

/*Table structure for table `sites` */

DROP TABLE IF EXISTS `sites`;

CREATE TABLE `sites` (
  `id` int(7) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `weeklydate` date NOT NULL COMMENT 'name of site',
  `Name` varchar(255) NOT NULL COMMENT 'weekly date of visit',
  `totalVisit` bigint(255) NOT NULL COMMENT 'total visit of site',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172220719 DEFAULT CHARSET=utf8;

/*Data for the table `sites` */

insert  into `sites`(`id`,`weeklydate`,`Name`,`totalVisit`) values (172220652,'2016-01-06','www.bing.com',14065457),(172220653,'2016-01-06','www.ebay.com.au',19831166),(172220654,'2016-01-06','www.facebook.com',104346720),(172220655,'2016-01-06','mail.live.com',21536612),(172220656,'2016-01-06','www.wikipedia.org',13246531),(172220657,'2016-01-27','www.ebay.com.au',23154653),(172220658,'2016-01-06','au.yahoo.com',11492756),(172220659,'2016-01-06','www.google.com',26165099),(172220660,'2016-01-13','www.youtube.com',68487810),(172220661,'2016-01-27','www.wikipedia.org',16550230),(172220662,'2016-01-06','ninemsn.com.au',21734381),(172220663,'2016-01-20','mail.live.com',24344783),(172220664,'2016-01-20','www.ebay.com.au',22598506),(172220665,'2016-01-27','mail.live.com',24272437),(172220666,'2016-01-27','www.bing.com',16041776),(172220667,'2016-01-20','ninemsn.com.au',24241574),(172220668,'2016-01-20','www.facebook.com',118984483),(172220669,'2016-01-27','ninemsn.com.au',24521168),(172220670,'2016-01-27','www.facebook.com',123831275),(172220671,'2016-01-20','www.bing.com',16595739),(172220672,'2016-01-13','www.facebook.com',118506019),(172220673,'2016-01-20','www.google.com.au',170020924),(172220674,'2016-01-27','www.youtube.com',69327140),(172220675,'2016-01-13','mail.live.com',24772355),(172220676,'2016-01-13','ninemsn.com.au',24555033),(172220677,'2016-01-20','www.google.com',28996455),(172220678,'2016-01-13','www.bing.com',16618315),(172220679,'2016-01-27','www.google.com.au',171842376),(172220680,'2016-01-06','www.youtube.com',59811438),(172220681,'2016-01-13','www.netbank.commbank.com.au',13316233),(172220682,'2016-01-20','www.netbank.commbank.com.au',13072234),(172220683,'2016-01-13','www.ebay.com.au',22785028),(172220684,'2016-01-20','www.wikipedia.org',16519992),(172220685,'2016-01-27','www.bom.gov.au',14369775),(172220686,'2016-01-27','www.google.com',29422150),(172220687,'2016-01-20','www.youtube.com',69064107),(172220689,'2016-01-13','www.wikipedia.org',16015926),(172220690,'2016-01-13','www.google.com',29203671),(172220691,'2016-01-13','www.google.com.au',172220397),(172220717,'2016-01-20','www.yahoo.fr',172220398),(172220718,'2017-03-03','www.yahoo.fr',172220397);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
